/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametersliders;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author maher
 */
public class SmartGeometry16SlidersController implements Initializable {

    @FXML
    public BorderPane gui;
    @FXML
    public Label hintLabel;
    @FXML
    public AnchorPane menuPane;
    @FXML
    public Label openSessionLabel;
    @FXML
    public Label saveSessionLabel;
    @FXML
    public Label streamSessionLabel;
    @FXML
    public Label pullGalleryLabel;
    @FXML
    public Label streamGalleryLabel;
    @FXML
    public Label plugAllLabel;
    @FXML
    public Label unPlugAllLabel;
    @FXML
    public Label clearSessionLabel;
    @FXML
    public Button saveAsSession;
    @FXML
    public Button saveSession;
    @FXML
    public Button pullGallery;
    @FXML
    public Button openSession;
    @FXML
    public Button expandMenu;

    @FXML
    public Button resetZoom;
    @FXML
    public VBox infoBox;
    @FXML
    public Label galleryFileLabel;
    @FXML
    public Label sessionFileLabel;
    public ZoomablePane sliderPane;

    @FXML
    public Label menuLabel;
    @FXML
    public Label saveAsSessionLabel;
    @FXML
    public ToggleButton streamSession;
    @FXML
    public ToggleButton streamGallery;
    @FXML
    public Button clearAll;
    @FXML
    public Button plugAll;
    @FXML
    public Button about;
    @FXML
    public Button unplugAll;
    @FXML
    public GridPane buttonGrid;
    @FXML
    public GridPane buttonLabelGrid;
    @FXML
    public HBox buttonHBox;

    @FXML
    public ScrollPane scrollPane;
    private DropShadow glow = new DropShadow();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sliderPane = new ZoomablePane();
        sliderPane.setStyle("-fx-background-color: white;");

        scrollPane.setContent(sliderPane);
        sliderPane.prefHeightProperty().bind(scrollPane.heightProperty());
        sliderPane.prefWidthProperty().bind(scrollPane.widthProperty());
        glow.setOffsetY(0);
        glow.setOffsetX(0);
        glow.setColor(Color.WHITE);
        glow.setWidth(20);
        glow.setHeight(20);
        // TODO
        openSession.setTooltip(makeTip("Open Session"));
        saveSession.setTooltip(makeTip("Save Session"));
        saveAsSession.setTooltip(makeTip("Save-As Session"));
        streamSession.setTooltip(makeTip("Stream Session"));
        pullGallery.setTooltip(makeTip("Pull From Gallery"));
        streamGallery.setTooltip(makeTip("Stream Gallery"));
        unplugAll.setTooltip(makeTip("Unplug ALl Sliders"));
        plugAll.setTooltip(makeTip("Plug All Sliders"));
        clearAll.setTooltip(makeTip("Delete All Sliders"));
        expandMenu.setTooltip(makeTip("Expand/Retract Menu"));
        openSession.setTooltip(makeTip("Open Session"));
        about.setTooltip(makeTip("About"));
        resetZoom.setTooltip(makeTip("Reset Zoom to 1:1"));
        style(openSession);
        style(saveSession);
        style(saveAsSession);
        style(streamSession);
        style(pullGallery);
        style(streamGallery);
        style(unplugAll);
        style(plugAll);
        style(clearAll);
        style(expandMenu);
        style(openSession);
        style(about);
        style(resetZoom);
    }

    private void style(Button node) {
        node.setOnMouseEntered((MouseEvent event) -> {
            node.getGraphic().setEffect(glow);
        });
        node.setOnMouseExited((MouseEvent event) -> {
            node.getGraphic().setEffect(null);
        });
    }

    private void style(ToggleButton node) {
        node.setOnMouseEntered((MouseEvent event) -> {
            node.getGraphic().setEffect(glow);
        });
        node.setOnMouseExited((MouseEvent event) -> {
            node.getGraphic().setEffect(null);
        });
    }

    private Tooltip makeTip(String tipTxt) {
        Tooltip tip = new Tooltip(tipTxt);

        tip.setStyle("-fx-font-family: arial; -fx-font-size:12;");
        return tip;
    }

}

class ZoomablePane extends AnchorPane {

    final double SCALE_DELTA = 1.1;

    public Group content = new Group();

    public ZoomablePane() {
        super();
        getChildren().add(content);
        content.setAutoSizeChildren(true);

    }

}
