/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametersliders;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author melkhaldi
 */
public class ParameterSliderController implements Initializable {

    @FXML
    public AnchorPane canvas;
    @FXML
    public AnchorPane titleBar;
    @FXML
    public ImageView plug;
    @FXML
    public TextField nameField;
    @FXML
    public ImageView close;
    @FXML
    public Slider slider;
    
   
    @FXML
    public ImageView expand;
    @FXML
    public TextField minField;
    public Label valueLabel1;
    @FXML
    public TextField maxField;
    public Label valueLabel11;
    @FXML
    public TextField valField;
    public Label valueLabel111;
    @FXML
    public AnchorPane details;
    
    @FXML
    public TextField roundField;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    
}
