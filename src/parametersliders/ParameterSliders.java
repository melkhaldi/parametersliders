/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametersliders;

import com.cedarsoftware.util.io.JsonObject;
import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import static javafx.scene.input.MouseButton.PRIMARY;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author melkhaldi
 */
public class ParameterSliders extends Application {

    double wScale, hScale;
    Stage dialog = new Stage();
    ParameterSlider tempSlider;
    Map<String, Number> sessionSlidersMap = new HashMap();
    DataOutputStream streamOUT;
    JsonWriter JSONwriter;
    boolean galleryMonitorON = false;
    boolean sessionMonitorON = false;
    final long pollingInterval = 500;

    FileAlterationMonitor sessionMonitor = new FileAlterationMonitor(pollingInterval);
    FileAlterationMonitor galleryMonitor = new FileAlterationMonitor(pollingInterval);
    FileFilter sessionFilter, galleryFilter;
    FileAlterationObserver sessionObserver, galleryObserver;
    FileAlterationListener sessionListener, galleryListener;
    SimpleIntegerProperty sliderMapStateChange = new SimpleIntegerProperty(0);
    FileChooser fileChooser = new FileChooser();
    File galleryFile = null;
    File sessionFile = null;
    String galleryReadString;
    HashMap<String, Object> galleryMap = new HashMap<>();
    HashMap<String, Number> galleryParamsMap = new HashMap<>();
    HashMap<String, Object> sessionMap = new HashMap<>();
    HashMap<String, Number> sessionParamsMap = new HashMap<>();
    String sessionReadString = "";
    ObservableList<ParameterSlider> sliderList = FXCollections.observableArrayList();

    AnchorPane root = new AnchorPane();

    Image Sg16, Sg16L;

    String openMessage = "Select Session File To Open. Sliders In Current Session That have Similar Names To Those in Selected Session File Will Be Overriden";
    String mergeMessage = "Select Gallery File To Merge With Existing Session. Sliders With Similar Names Will Be Overriden";
    String saveMessage = "Save Session File To Disk";

    String galleryParamString = "";
    String sessionParamString = "";
    ArrayList<Node> unwantedNodes = new ArrayList();

    private SmartGeometry16SlidersController controller = new SmartGeometry16SlidersController();
    private FXMLLoader loader = new FXMLLoader(this.getClass().getResource("SmartGeometry16Sliders.fxml"));
    String tempString = "";
    int nameHits = 0;
    AnchorPane dialogPane = new AnchorPane();

    public void LoadGui() {
        loader.setController(controller);
        try {
            root = (AnchorPane) loader.load();
        } catch (IOException ex) {
            Logger.getLogger(ParameterSlider.class.getName()).log(Level.SEVERE, null, ex);
        }

//      
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setOnCloseRequest((WindowEvent event) -> {
            System.exit(0);
            Platform.exit();
            primaryStage.close();
        });

        LoadGui();
        sessionMap.put("params", sessionSlidersMap);
        fileChooser.setTitle(mergeMessage);
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.JSON"));

        Sg16 = new Image(ParameterSliders.class.getResourceAsStream("sg2016parallelparam.png"));
        Sg16L = new Image(ParameterSliders.class.getResourceAsStream("sg2016parallelparam_background.png"));

        primaryStage.setResizable(true);
        primaryStage.getIcons().add(Sg16);
        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(500);

        controller.streamGallery.setPadding(new Insets(3));
        controller.streamSession.setPadding(new Insets(3));
        controller.streamGallery.setDisable(true);
        controller.streamSession.setDisable(true);
        controller.streamSession.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            controller.saveAsSession.setDisable(newValue);
            controller.saveSession.setDisable(newValue);
            controller.saveAsSessionLabel.setDisable(newValue);
            controller.saveSessionLabel.setDisable(newValue);
            if (newValue) {
                controller.streamSession.setStyle("-fx-background-color:  #505050");
                writeSession();
            } else {
                controller.streamSession.setStyle("-fx-background-color:  #808080");
            }
        });
        controller.streamGallery.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            controller.pullGallery.setDisable(newValue);
            controller.pullGalleryLabel.setDisable(newValue);
            if (newValue) {
                controller.streamGallery.setStyle("-fx-background-color:  #505050");
                parseGallery();
            } else {
                controller.streamGallery.setStyle("-fx-background-color:  #808080");
            }
            disableConnection(!newValue);
        });
        controller.saveAsSession.setOnMouseClicked((MouseEvent event) -> {
            if (createSessionFile(primaryStage)) {
                writeSession();
            }
        });
        controller.clearAll.setOnMouseClicked((MouseEvent event) -> {
            clearAll();
            if (controller.streamSession.isSelected() && sessionFile != null) {
                writeSession();
            }
        });
        controller.saveSession.setOnMouseClicked((MouseEvent event) -> {
            if (sessionFile == null) {
                createSessionFile(primaryStage);
            }
            writeSession();
        });

        controller.expandMenu.setOnMouseClicked((MouseEvent event) -> {
            hideLabelsFromMenuBar((controller.buttonHBox.getChildren().contains(controller.buttonLabelGrid)));
        });

        controller.openSession.setOnMouseClicked((MouseEvent event) -> {
            openSessionFile(primaryStage);
        });

        controller.pullGallery.setOnAction((ActionEvent event) -> {
            try {
                if (setGalleryFile(primaryStage)) {
                    if (galleryFile != null) {
                        parseGallery();
                        controller.streamGallery.setDisable(false);
                    }
                }
            } catch (Exception e) {
            }
        });

        Scene scene = new Scene(root, 780, 480);

        controller.unplugAll.setOnMouseClicked((MouseEvent event) -> {
            plugAll(false);
        });
        controller.plugAll.setOnMouseClicked((MouseEvent event) -> {
            plugAll(true);
        });
        sliderList.addListener((ListChangeListener.Change<? extends ParameterSlider> c) -> {
            sliderList.forEach(slider -> {
                if (!controller.sliderPane.content.getChildren().contains(slider)) {
                    controller.sliderPane.content.getChildren().add(slider);
                }
            });

            sliderMapStateChange.set(sliderMapStateChange.get() + 1);
        });

        sliderMapStateChange.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            controller.sliderPane.content.getChildren().stream().forEach((Node child) -> {
                if (!sliderList.contains(child)) {
                    unwantedNodes.add(child);
                }
            });

            controller.sliderPane.content.getChildren().removeAll(unwantedNodes);
            if (controller.streamSession.isSelected() && sessionFile != null) {
                writeSession();
            }

            controller.clearAll.setDisable(sliderList.isEmpty());
            controller.plugAll.setDisable(sliderList.isEmpty());
            controller.unplugAll.setDisable(sliderList.isEmpty());
            controller.resetZoom.setDisable(sliderList.isEmpty());
        });
        primaryStage.setTitle("SmartGeometry 2016. Parallel Parametrics Sliders. Beta 5");
        primaryStage.setScene(scene);
        primaryStage.show();

        controller.scrollPane.setOnMouseClicked((MouseEvent event) -> {
            controller.scrollPane.requestFocus();

            if (event.getButton().equals(PRIMARY) && event.getClickCount() == 2) {
                addSlider("", "1", false);
                sliderList.get(sliderList.size() - 1).setLayoutX(event.getSceneX() - controller.sliderPane.getLayoutX() - sliderList.get(sliderList.size() - 1).width / 2);
                sliderList.get(sliderList.size() - 1).setLayoutY(event.getSceneY() - controller.sliderPane.getLayoutY() - sliderList.get(sliderList.size() - 1).shortHeight / 2);
                sliderList.get(sliderList.size() - 1).posX = sliderList.get(sliderList.size() - 1).getLayoutX();
                sliderList.get(sliderList.size() - 1).posY = sliderList.get(sliderList.size() - 1).getLayoutY();
            }
        });

        controller.sliderPane.setOnScroll((ScrollEvent event) -> {
            event.consume();
            if (event.getDeltaY() == 0) {
                return;
            }

            double scaleFactor
                    = (event.getDeltaY() > 0)
                            ? controller.sliderPane.SCALE_DELTA
                            : 1 / controller.sliderPane.SCALE_DELTA;

            controller.sliderPane.content.setScaleX(controller.sliderPane.content.getScaleX() * scaleFactor);
            controller.sliderPane.content.setScaleY(controller.sliderPane.content.getScaleY() * scaleFactor);
        });

        controller.about.setOnMouseClicked((MouseEvent event) -> {
            dialog.showAndWait();
        });

        controller.resetZoom.setOnMouseClicked((MouseEvent event) -> {
            controller.sliderPane.content.setScaleX(1);
            controller.sliderPane.content.setScaleY(1);
        });

        BackgroundImage backgroundImage = new BackgroundImage(Sg16L, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        BackgroundFill fill = new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY);
        ArrayList<BackgroundFill> fills = new ArrayList();
        ArrayList<BackgroundImage> images = new ArrayList();
        fills.add(fill);
        images.add(backgroundImage);
        Background background = new Background(fills, images);

        controller.plugAll.setDisable(true);
        controller.unplugAll.setDisable(true);
        controller.clearAll.setDisable(true);
        controller.resetZoom.setDisable(true);
        dialog.setWidth(500);
        dialog.setHeight(350);
        dialog.initStyle(StageStyle.UTILITY);
        Scene diaglogScene = new Scene(dialogPane);
        dialog.setScene(diaglogScene);

        dialogPane.setBackground(background);
        dialog.setResizable(false);
        dialog.setTitle("About Smart Geometry 2016. Parallel Parametrics Sliders");
        VBox credits = new VBox();
        credits.setSpacing(25);
        credits.setStyle("-fx-font-family: arial; -fx-font-size: 12;-fx-font-weight:bold;");
        dialogPane.getChildren().add(credits);
        credits.setPadding(new Insets(20));
        credits.getChildren().add(new Label("Developed For: SmartGeometry 2016. Parallel Parametrics Workshop."));
        credits.getChildren().add(new Label("Chamions: Robert Woodbury and Mark Cichy"));
        credits.getChildren().add(new Label("Experts: Lars Hesselgren and Volker Mueller"));
        credits.getChildren().add(new Label("Cluster Experts: Arefin Mohiuddin and Maher Elkhaldi"));
        credits.getChildren().add(new Label("App Developer: Maher Elkhaldi. msamiahm@sfu.ca"));
        credits.getChildren().add(new Label("Libraries:"
                + "\n\tApache Lang: https://commons.apache.org/proper/commons-lang/"
                + "\n\tApache IO http://commons.apache.org/proper/commons-io/"
                + "\n\tJSON-IO https://github.com/jdereg/json-io.git"));
        credits.getChildren().add(new Label("Resources: button icons from: https://icons8.com/"));

        hideLabelsFromMenuBar(true);
    }

    private boolean setGalleryFile(Stage stage) {
        fileChooser.setTitle(mergeMessage);
        File f = fileChooser.showOpenDialog(stage);
        if (f != null) {
            galleryFile = f;
            controller.galleryFileLabel.setText("Gallery File: " + galleryFile.getAbsoluteFile());
            watchGalleryFile();
            return true;
        }
        return false;
    }

    private void clearAll() {
        sliderList.clear();
        sessionParamsMap.clear();
    }

    private void plugAll(boolean val) {
        sliderList.stream().forEach(slider -> {
            slider.plug(val);
        });
    }

    private void hideLabelsFromMenuBar(boolean val) {
        if (val) {
            controller.buttonHBox.getChildren().remove(controller.buttonLabelGrid);
        } else {
            controller.buttonHBox.getChildren().add(controller.buttonLabelGrid);
        }
    }

    private boolean openSessionFile(Stage stage) {
        fileChooser.setTitle(openMessage);
        File f = fileChooser.showOpenDialog(stage);
        if (f != null) {
            sessionFile = f;
            parseSession();
            controller.sessionFileLabel.setText("Session File: " + sessionFile.getAbsoluteFile());
            watchSessionFile();
            return true;
        };
        return false;
    }

    private boolean createSessionFile(Stage stage) {
        fileChooser.setTitle(saveMessage);
        File f = fileChooser.showSaveDialog(stage);
        if (f != null) {
            sessionFile = f;
            try {
                sessionFile.createNewFile();
                controller.sessionFileLabel.setText("Session File: " + sessionFile.getAbsoluteFile());
                watchSessionFile();
            } catch (IOException ex) {
                Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        };
        return false;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private FileAlterationListener setSessionMonitorListener() {
        return new FileAlterationListenerAdaptor() {
            @Override
            public void onFileDelete(File file) {
                sessionFile = null;
                controller.streamSession.setSelected(false);
                controller.streamSession.setDisable(true);
                sessionObserver.removeListener(this);
                try {
                    sessionMonitor.stop();
                    sessionMonitorON = false;
                } catch (Exception ex) {
                    Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.runLater(() -> {
                    controller.sliderPane.requestFocus();
                    controller.sessionFileLabel.setText("Session File: Undefined.");
                });

            }
        };
    }

    private FileAlterationListener setGalleryListener() {
        return new FileAlterationListenerAdaptor() {
            @Override
            public void onFileChange(File file) {
                if (controller.streamGallery.isSelected()) {
                    Platform.runLater(() -> {
                        parseGallery();
                        removeUnusedPlugs();
                    });
                }
            }

            @Override
            public void onFileDelete(File file) {
                galleryFile = null;
                controller.streamGallery.setSelected(false);
                controller.streamGallery.setDisable(true);
                galleryObserver.removeListener(this);
                try {
                    galleryMonitor.stop();
                    galleryMonitorON = false;
                } catch (Exception ex) {
                    Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.runLater(() -> {
                    controller.sliderPane.requestFocus();
                    controller.galleryFileLabel.setText("Gallery File: Undefined.");
                    removeUnusedPlugs();
                });
            }
        };
    }

    private void setSessionMonitor() {
        try {
            if (sessionMonitorON) {
                sessionMonitor.removeObserver(sessionObserver);
                sessionMonitor.stop();
            }

            sessionFilter = new NameFileFilter(sessionFile.getName());
            sessionObserver = new FileAlterationObserver(sessionFile.getParent(), sessionFilter);
            sessionMonitor.addObserver(sessionObserver);
            sessionListener = setSessionMonitorListener();
            sessionObserver.addListener(sessionListener);
        } catch (Exception ex) {
            Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void watchSessionFile() {
        controller.streamSession.setDisable(false);
        try {
            setSessionMonitor();
            sessionMonitor.start();
            sessionMonitorON = true;
        } catch (Exception ex) {
            Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setGalleryMonitor() {
        try {
            if (galleryMonitorON) {
                galleryMonitor.removeObserver(galleryObserver);
                galleryMonitor.stop();
            }

            galleryFilter = new NameFileFilter(galleryFile.getName());
            galleryObserver = new FileAlterationObserver(galleryFile.getParent(), galleryFilter);
            galleryListener = setGalleryListener();
            galleryObserver.addListener(galleryListener);
            galleryMonitor.addObserver(galleryObserver);
        } catch (Exception ex) {
            Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void watchGalleryFile() {
        try {
            setGalleryMonitor();
            galleryMonitor.start();
            galleryMonitorON = true;
        } catch (Exception ex) {
            Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean foundDuplicate(String checkName) {
        nameHits = 0;
        sliderList.stream().forEach(slider -> {
            if (slider.nameProperty().get().equals(checkName)) {
                nameHits += 1;
            }
        });

        return nameHits > 1;
    }

    private void addOrSetSlider(String sliderName, Number sliderValue) {
        if (getSlider(sliderName) != null) {
            tempSlider = getSlider(sliderName);
            if (tempSlider.parameterValueProperty().get() != sliderValue.doubleValue()) {
                setSlider(sliderValue.toString(), getSlider(sliderName));

            }

        } else {
            addSlider(sliderName, sliderValue.toString(), true);
        }

        getSlider(sliderName).addPlug();
        getSlider(sliderName).disableNameField(true);
    }

    private boolean existsInGallery(ParameterSlider slider) {
        if (galleryFile != null) {
            galleryParamsMap.clear();
            fillSessionMapFromFile(galleryFile, galleryParamsMap);
            if (galleryParamsMap.containsKey(slider.nameProperty().get())) {
                return true;
            }
        }

        return false;
    }

    private void removeUnusedPlugs() {
        sliderList.forEach(slider -> {
            if (!existsInGallery(slider)) {
                slider.removePlug();
                slider.disableNameField(false);
            }
        });
    }

    private void setSessionMap() {
        setSliderValueMap();
        sessionMap.put("params", sessionSlidersMap);
    }

    private void cleanUp(File file) {
        try {
            System.out.println("cleaning up");
            String rawText = getFileText(file).trim();
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            String cleanedUP = StringUtils.remove(rawText, "\"@type\":\"java.util.HashMap\",");
            bw.write(cleanedUP);
            bw.flush();
        } catch (IOException ex) {
            Logger.getLogger(ParameterSliders.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeSession() {
        try {
            if (sessionFile != null) {
                DataOutputStream stream = new DataOutputStream(new FileOutputStream(sessionFile));
                JsonWriter jw = new JsonWriter(stream);
                setSessionMap();

                jw.write(sessionMap);

                cleanUp(sessionFile);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParameterSliders.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void parseSession() {
        fillSessionMapFromFile(sessionFile, sessionParamsMap);
        sessionParamMapToSliders();
        removeUnusedPlugs();
    }

    private void parseGallery() {
        fillSessionMapFromFile(galleryFile, sessionParamsMap);
        sessionParamMapToSliders();
        if (controller.streamSession.isSelected()) {
            writeSession();
        }
        removeUnusedPlugs();
    }

    private void fillSessionMapFromFile(File file, HashMap<String, Number> map) {
        JsonObject tempMap = new JsonObject();
        tempMap.putAll(JsonReader.jsonToMaps(getFileText(file)));
        tempMap.keySet().stream().forEach(key -> {
            if (key.equals("params")) {
                HashMap<String, Number> params = (HashMap<String, Number>) tempMap.get(key);
                map.putAll(params);
            }
        });
    }

    private void sessionParamMapToSliders() {
        sessionParamsMap.keySet().forEach(key -> {
            addOrSetSlider(key, sessionParamsMap.get(key));
        });
    }

    private String getFileText(File file) {
        StringBuilder stringBuffer = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                stringBuffer.append(text);
            }
            bufferedReader.close();

        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(ParameterSliders.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ParameterSliders.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return stringBuffer.toString();

    }

    private void setSliderValueMap() {
        sessionSlidersMap.clear();
        sliderList.stream().forEach(slider -> {
            sessionSlidersMap.put(slider.nameProperty().get(), slider.parameterValueProperty().get());
        });
    }

    private ParameterSlider getSlider(String name) {
        for (ParameterSlider slider : sliderList) {
            if (slider.nameProperty().get().equals(name)) {
                return slider;
            }
        }
        return null;
    }

    private void setSlider(String value, ParameterSlider slider) {
        if (slider.plugProperty.get()) {
            slider.adjustValue(value);
        }
    }

    private void addSlider(String name, String value, boolean nameFieldDisable) {
        ParameterSlider parameterSlider = new ParameterSlider(name, value);

        parameterSlider.disableNameField(nameFieldDisable);
        parameterSlider.parameterValueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            if (controller.streamSession.isSelected()) {
                writeSession();
            }
        });

        parameterSlider.plugProperty.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                if (controller.streamGallery.isSelected()) {
                    parseGallery();
                }
            }
        });

        parameterSlider.closeProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            sliderList.remove(parameterSlider);
            sessionParamsMap.remove(parameterSlider.nameProperty().get());
            controller.sliderPane.content.getChildren().remove(parameterSlider.root);
            if (controller.streamSession.isSelected()) {
                writeSession();
            }
        });

        parameterSlider.nameProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            System.out.println("name changed");
            if (foundDuplicate(newValue)) {
                parameterSlider.useLastName();
            } else if (controller.streamSession.isSelected()) {
                writeSession();
            }
        });
        sliderList.add(parameterSlider);
    }

    private void disableConnection(boolean val) {
        sliderList.forEach(slider -> {
            slider.disablePlug(val);
        });
    }
}
