/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametersliders;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.image.Image;
import static javafx.scene.input.KeyCode.ENTER;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author melkhaldi
 */
public final class ParameterSlider extends Group {

    String tempValue = "";
    Image plugged, unplugged;
    String lastUsedValue = "0";
    String lastUsedMin = "2";
    String lastUsedMax = "-2";
    String lastUsedFormat = "#.#";

    String shadowON = "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.5) , 5, 0.2 , 0 , 1  );";
    String shadowOFF = "-fx-effect: dropshadow( gaussian , rgba(0,0,0,0.5) , 0,0,0,0 );";

    private ReadOnlyDoubleWrapper parameterValueProperty = new ReadOnlyDoubleWrapper(0);
    private ReadOnlyStringWrapper nameProperty = new ReadOnlyStringWrapper();
    private ReadOnlyStringWrapper textFieldProperty = new ReadOnlyStringWrapper();

    public String lastUsedName = "";
    public ReadOnlyStringWrapper formatStringProperty = new ReadOnlyStringWrapper(lastUsedValue);

    private ReadOnlyBooleanWrapper close = new ReadOnlyBooleanWrapper(false);
    DecimalFormat valFormatter;
    public final double shortHeight = 100;
    public final double tallHeight = 130;
    public final double width = 300;
    private double mouseX, mouseY, deltaX, deltaY;
    public double posX, posY;
    public SimpleBooleanProperty plugProperty = new SimpleBooleanProperty(true);

    double dblVal = 1.0;
    int intVal = 1;

    private void initValue(String valString) {
        lastUsedFormat = "#";
        if (isNumber(valString.trim())) {
            lastUsedValue = valString;
            if (StringUtils.contains(valString, '.')) {
                int zeros = StringUtils.substringAfter(valString, ".").length();
                lastUsedFormat = "#.";
                for (int i = 0; i < zeros; i++) {
                    lastUsedFormat += "#";
                }
            }
        } else {

            lastUsedValue = "1";
        }
        controller.roundField.setText(lastUsedFormat);
        valFormatter = new DecimalFormat(lastUsedFormat);
        formatStringProperty.set(lastUsedValue);
        controller.valField.setText(lastUsedValue);
        controller.slider.setValue(Double.valueOf(lastUsedValue));

        textFieldProperty.bind(controller.nameField.textProperty());
        Platform.runLater(() -> {
            controller.nameField.requestFocus();
        });
        parameterValueProperty.set(controller.slider.getValue());

    }

    public void setScale(double factor) {
        this.setScaleX(this.getScaleX() * factor);
        this.setScaleY(this.getScaleY() * factor);
    }

    public void resetScale() {
        this.setScaleX(1);
        this.setScaleY(1);
    }

    public ReadOnlyStringProperty textFieldProperty() {
        return textFieldProperty.getReadOnlyProperty();
    }

    public void initOtherValues() {

        double min, max;
        if (controller.slider.getValue() > 0) {
            min = controller.slider.getValue() * -2;
            max = controller.slider.getValue() * 2;
        } else if (controller.slider.getValue() < 0) {
            min = controller.slider.getValue() * 2;
            max = controller.slider.getValue() * -2;
        } else {
            max = controller.slider.getValue() + 10;
            min = controller.slider.getValue() - 10;
        }
        controller.minField.setText(min + "");

        controller.slider.setMax(max);
        controller.slider.setMin(min);

        controller.maxField.setText(max + "");
        lastUsedMax = max + "";
        lastUsedMin = min + "";
    }

    public ReadOnlyStringProperty nameProperty() {
        return nameProperty.getReadOnlyProperty();
    }

    public ReadOnlyDoubleProperty parameterValueProperty() {
        return parameterValueProperty.getReadOnlyProperty();
    }

    public final void disableNameField(boolean value) {
        controller.nameField.setDisable(value);
    }
    private ParameterSliderController controller = new ParameterSliderController();
    private FXMLLoader loader = new FXMLLoader(this.getClass().getResource("parameterSlider.fxml"));

    public AnchorPane root;

    public ReadOnlyBooleanProperty closeProperty() {
        return close.getReadOnlyProperty();
    }

    private String makeFormatLegal(String string) {
        if (StringUtils.containsOnly(string, '#', '.')) {
            String temp = string;
            if (StringUtils.endsWith(string, ".")) {
                temp = StringUtils.appendIfMissing(string, "#");
            }
            if (StringUtils.startsWith(string, ".")) {
                temp = StringUtils.prependIfMissing(string, "#");
            }

            return temp;
        } else {
            return lastUsedFormat;
        }
    }

    private Number validateValue(String txt) {
        String toProcess = txt;
        if (StringUtils.endsWith(txt, ".")) {
            toProcess = StringUtils.substringBefore(txt, ".");
        }
        if (isNumber(toProcess)) {
            if (toProcess.contains(".")) {
                return Double.valueOf(makeSenseOutOf(toProcess));
            } else {
                return Integer.valueOf(makeSenseOutOf(toProcess));
            }
        } else {
            return null;
        }
    }

    private String makeSenseOutOf(String val) {
        if (val.equals("-0")) {
            return "0";
        } else {
            return val;
        }
    }

    ParameterSlider(String name, String value) {
        loader.setController(controller);
        try {
            root = loader.load();
            getChildren().add(root);
        } catch (IOException ex) {
            Logger.getLogger(ParameterSlider.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (name.equals("")) {
            name = "<" + ParameterSlider.this.hashCode() + ">";

        }
        lastUsedName = name;
        nameProperty.set(name);
        controller.nameField.setText(name);
        initValue(value);
        initOtherValues();

        nameProperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            lastUsedName = oldValue;
            if (!nameLegal(newValue)) {
                setNameField(lastUsedName);
            }
        });
        unplugged = new Image(ParameterSlider.class.getResourceAsStream("Disconnected.png"));
        plugged = new Image(ParameterSlider.class.getResourceAsStream("Connected.png"));
        formatStringProperty.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            valFormatter = new DecimalFormat(newValue);
            adjustValue(valFormatter.format(validateValue(controller.valField.getText())));
        });

        controller.roundField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(ENTER)) {
                fireRoundField();
            }
        });

        controller.expand.setOnMouseClicked((MouseEvent event) -> {
            if (controller.canvas.getPrefHeight() == tallHeight) {
                collapse();
            } else {
                expand();
            }
        });

        controller.close.setOnMouseClicked((MouseEvent event) -> {
            close.set(true);
        });

        controller.slider.setOnMousePressed((MouseEvent event) -> {
            controller.valField.setText(makeSenseOutOf(valFormatter.format(validateValue(controller.slider.getValue() + ""))));
            adjustValue(valFormatter.format(validateValue(controller.valField.getText())));
        });
        controller.slider.setOnMouseDragged((MouseEvent event) -> {
            if (controller.slider.getValue() < 0) {
                valFormatter.setRoundingMode(RoundingMode.CEILING);
            } else {
                valFormatter.setRoundingMode(RoundingMode.FLOOR);
            }
            controller.valField.setText(makeSenseOutOf(valFormatter.format(validateValue(controller.slider.getValue() + ""))));
        });

        controller.slider.setOnMouseReleased((MouseEvent event) -> {
            adjustValue(valFormatter.format(validateValue(controller.valField.getText())));
        });

        controller.plug.setOnMouseClicked((MouseEvent event) -> {
            if (plugProperty.get()) {
                plugProperty.set(false);
                controller.plug.setImage(unplugged);
            } else {
                controller.plug.setImage(plugged);
                plugProperty.set(true);
            }

        });

        controller.plug.setOnMouseEntered((MouseEvent event) -> {
            controller.plug.setStyle(shadowON);
        });
        controller.plug.setOnMouseExited((MouseEvent event) -> {
            controller.plug.setStyle(shadowOFF);
        });
        controller.close.setOnMouseEntered((MouseEvent event) -> {
            controller.close.setStyle(shadowON);
        });
        controller.close.setOnMouseExited((MouseEvent event) -> {
            controller.close.setStyle(shadowOFF);
        });
        controller.expand.setOnMouseEntered((MouseEvent event) -> {
            controller.expand.setStyle(shadowON);

        });
        controller.expand.setOnMouseExited((MouseEvent event) -> {
            controller.expand.setStyle(shadowOFF);

        });

        controller.valField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(ENTER)) {
                fireValField();
            }
        });

        controller.minField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(ENTER)) {
                if (Double.valueOf(controller.minField.getText()) < Double.valueOf(controller.maxField.getText())) {
                    fireMinField();
                } else {
                    controller.minField.setText(lastUsedMin);
                }
            }
        });

        controller.maxField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(ENTER)) {
                if (Double.valueOf(controller.minField.getText()) < Double.valueOf(controller.maxField.getText())) {
                    fireMaxField();
                } else {
                    controller.maxField.setText(lastUsedMax);
                }
            }
        });

        setOnMousePressed((MouseEvent event) -> {
            mouseX = event.getSceneX();
            mouseY = event.getSceneY();
            toFront();
        });
        setOnMouseDragged((MouseEvent event) -> {
            deltaX = event.getSceneX() - mouseX + posX;
            deltaY = event.getSceneY() - mouseY + posY;
            setLayoutX(deltaX);
            setLayoutY(deltaY);
        });
        setOnMouseReleased((MouseEvent event) -> {
            posX = getLayoutX();
            posY = getLayoutY();
        });

        controller.nameField.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode().equals(ENTER) && !controller.nameField.isDisabled()) {
                fireNameField();
            }
        });

        controller.valField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                fireValField();
            }
        });
        controller.maxField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                fireMaxField();
            }
        });
        controller.minField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                fireMinField();
            }
        });
        controller.nameField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                fireNameField();
            }
        });
        controller.roundField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                fireRoundField();
            }
        });
        collapse();

    }

    private void fireRoundField() {
        lastUsedFormat = makeFormatLegal(controller.roundField.getText());
        controller.roundField.setText(lastUsedFormat);
        formatStringProperty.set(lastUsedFormat);
        controller.roundField.forward();
        adjustValue(makeSenseOutOf(valFormatter.format(validateValue(controller.slider.getValue() + ""))));
        
    }

    private void collapse() {
        controller.expand.setRotate(0);
        controller.details.setVisible(false);
        controller.canvas.setPrefHeight(shortHeight);
    }

    private void expand() {
        controller.expand.setRotate(90);
        controller.details.setVisible(true);
        controller.canvas.setPrefHeight(tallHeight);
    }

    private void fireMaxField() {
        lastUsedMax = validateValue(controller.maxField.getText()).toString();
        controller.maxField.setText(lastUsedMax);
        controller.slider.setMax(Double.valueOf(lastUsedMax));
        if (controller.slider.getMax() < Double.valueOf(lastUsedValue)) {
            valFormatter.setRoundingMode(RoundingMode.FLOOR);
            adjustValue(valFormatter.format(Double.valueOf(lastUsedMax)));
        }
        controller.maxField.forward();
        
    }

    private void fireMinField() {
        lastUsedMin = validateValue(controller.minField.getText()).toString();
        controller.minField.setText(lastUsedMin);
        controller.slider.setMin(Double.valueOf(lastUsedMin));
        if (controller.slider.getMin() > Double.valueOf(lastUsedValue)) {
            valFormatter.setRoundingMode(RoundingMode.FLOOR);
            adjustValue(valFormatter.format(Double.valueOf(lastUsedMin)));

        }
        controller.minField.forward();
    }

    private void fireValField() {
        adjustValue(valFormatter.format(validateValue(controller.valField.getText())));
        controller.valField.forward();
    }

    private void fireNameField() {
        nameProperty.set(controller.nameField.getText());
        controller.nameField.forward();
    }

    private boolean isNumber(String strNum) {
        if (strNum.matches("-?\\d+(\\.\\d+)?")) {
            return true;
        }
        return false;
    }

    public void useLastName() {
        setNameField(lastUsedName);
    }

    private boolean nameLegal(String newName) {
        if (StringUtils.isBlank(newName) || StringUtils.isEmpty(newName)) {
            return false;
        }
        return true;
    }

    public final void setNameField(String newName) {
        this.controller.nameField.setText(newName);
        nameProperty.set(newName);
    }

    public void adjustValue(String valString) {

        if (controller.roundField.getText().contains(".")) {
            dblVal = Double.valueOf(valFormatter.format(validateValue(valString)));
            controller.valField.setText(dblVal+"");
            controller.slider.adjustValue(dblVal);
            parameterValueProperty.set(dblVal);
            lastUsedValue = controller.valField.getText();
            
        } else {
            intVal = Integer.valueOf(valFormatter.format(validateValue(valString)));
            controller.valField.setText(intVal+"");
            controller.slider.adjustValue(intVal);
            parameterValueProperty.set(intVal);
            lastUsedValue = controller.valField.getText();
        }

    }

    public void plug(boolean val) {
        plugProperty.set(val);
        if (val) {
            controller.plug.setImage(plugged);
        } else {
            controller.plug.setImage(unplugged);
        }
    }

    public void addPlug() {
        controller.plug.setVisible(true);
    }

    public void removePlug() {
        controller.plug.setVisible(false);

    }

    public void disablePlug(boolean val) {
        if (val) {
            plug(val);
            controller.plug.setDisable(true);
            controller.plug.setOpacity(0.25);
        } else {
            controller.plug.setDisable(false);
            controller.plug.setOpacity(1);
        }
    }

}
